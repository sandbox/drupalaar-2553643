<?php

/**
 * @file
 * Functions for Commerce Ogone Alias module that do not need to load during Drupal bootstrap module.
 */

/**
 * Executes the DirectLink payment for orders with a DirectLink status
 * (see Config form)
 * You need to create this status, or use the Commerce License Billing module
 *
 * Will be used in Cron task
 */
function commerce_ogone_alias_directlink_payments () {

  $statusses = variable_get('ogone_directlink_statusses') ;

  $statusses = array_filter($statusses) ;

  foreach ( $statusses as $status ) {

  $orders = commerce_order_load_multiple(array(), array('status' => $status), TRUE);

  watchdog('Ogone DirectLink', t(
    'Processing @count orders in status @status',
    array(
      '@count' => count($orders),
      '@status' => $status,
      )));

  foreach ($orders as $order) {

    $directlink_feedback = commerce_ogone_alias_directlink_payment($order);

    $successful = array (
      '5',
      '9',
    ) ;
    if ( array_search($directlink_feedback['STATUS'], $successful ) ) {

    commerce_ogone_alias_save_payment($order, $directlink_feedback);

    commerce_order_status_update(
      $order,
      'completed',
      FALSE,
      TRUE,
      $log = t(
        'Payment through DirectLink (Ogone payid = %payid)',
        array(
          '%ordernumber' => $order->order_number,
          '%payid' => $directlink_feedback['PAYID']
        )));

      rules_invoke_event('commerce_checkout_complete', $order );

      watchdog('Ogone DirectLink', t('Succesfull payment request for order %id', array('%id' => $order->order_number)));
    }
    else
    {
      watchdog('Ogone DirectLink', t('Payment request for order %id failed', array('%id' => $order->order_number)));
    }
  }
  }
}

/**
 * Saves the payment if payment was succesful.
 * @param $order
 * @param $directlink_feedback
 */
function commerce_ogone_alias_save_payment($order, $directlink_feedback) {

  /**
   * @todo it may be relevant to save failed payment request as well
   */

  $successful = array (
    '5', // Authorised
    '9') ; // Payment requested

  if ( array_search( $directlink_feedback ['STATUS'] , $successful ) ) {

    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    //Amount that will be charged
    $amount = $order_wrapper->commerce_order_total->amount->value();
    $currency_code = $order_wrapper->commerce_order_total->currency_code->value();

    $transaction = commerce_payment_transaction_new('commerce_payment_ogone', $order->order_id);
    $transaction->instance_id = 'Ogone DirectLink';
    $transaction->amount = $amount;
    $transaction->currency_code = $currency_code;
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = 'Ogone Payment ID: @payid';
    $transaction->message_variables = array('@payid' => $directlink_feedback['PAYID']);

    commerce_payment_transaction_save($transaction);
  }
}

/**
 * Sends the payment request to Ogone's DirectLink service
 * @param $postdata Payment Request string that must be submitted
 * @return Array with Ogone payment feedback parameters
 */
function _ogone_alias_payment_request($postdata) {

  /**
   *
   * Transmit the payment request and returns Ogone's feedback as array
   */
  $response = drupal_http_request($url, array(
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      'data' => $postdata,
      'method' => 'POST',
      'timeout' => 10,
    )
  );

  if ($response->status_message == 'OK') {

    //Convert the resulting XML to an array because we love arrays in Drupal
    $payment_transaction_data = new SimpleXMLElement($response->data);
    $payment_transaction_data = drupal_json_decode(drupal_json_encode($payment_transaction_data));

  }

  return $payment_transaction_data;

}

/**
 * Function to process an Ogone Direct Link payment
 */
function commerce_ogone_alias_directlink_payment($order) {

  watchdog('Ogone DirectLink', "Payment Transaction started");

  /*
   * Use the correct url for production or test
   */
  $transaction_mode = variable_get('ogone_directlink_mode');

  switch ($transaction_mode) {

    case 'production':
      $url = variable_get('ogone_directlink_url_production');
      break;

    default:
      $url = variable_get('ogone_directlink_url_test');
  };

  /*
   * Get the post request string required for an Ogone DirectLink payment
   */
  $postdata = _commmerce_ogone_alias_build_directlink_postdata($order);

  $response = drupal_http_request($url, array(
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      'data' => $postdata,
      'method' => 'POST',
      'timeout' => 10,
    )
  );

  if ($response->status_message == 'OK') {

    //Convert the resulting XML to an array because we love arrays in Drupal
    $payment_transaction_data = new SimpleXMLElement($response->data);
    $payment_transaction_data = drupal_json_decode(drupal_json_encode($payment_transaction_data));

  }

  return $payment_transaction_data['@attributes'];

}

/**
 * Constructs the data string to transmit as a http post request.
 * The string must follow the rules of Ogone's DirectLink api.
 * See https://secure.ogone.com/Ncol/Ogone_DirectLink_EN.pdf
 * @param $order Drupal Commerce order
 * @return string valid Ogone DirectLink request string
 */
function _commmerce_ogone_alias_build_directlink_postdata($order) {

  /**
   * 1.
   * Collect all required data
   * @todo get correct values from order
   */

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  //Amount that will be charged
  $amount = $order_wrapper->commerce_order_total->amount->value();
  $currency_code = $order_wrapper->commerce_order_total->currency_code->value();

  global $language;
  $payment_language = $language->language;

  $order_uid = $order->uid;

  // Build the data array that will be translated into hidden form values.
  // NOTE: keys should be uppercase for the SHA-1 security string
  $data = array(
    // General parameters
    'PSPID' => variable_get('directlink_pspid'),
    'ORDERID' => $order->order_number, //$order->order_number,
    'AMOUNT' => $amount,
    'USERID' => variable_get('ogone_directlink_api_user'),
    'PSWD' => variable_get('ogone_directlink_api_pswd'),
    'CURRENCY' => variable_get('commerce_default_currency', 'EUR'),
    'LANGUAGE' => $payment_language,
    'ALIAS' => t(ALIAS_TEMPLATE, array('@uid' => $order_uid)),
  );


  /**
   * 2. Build the request_string that Ogone DirectLink expects
   */

  // Ogone expects sorted data
  ksort($data);

  /**
   * We build a string with readable values and one that will be used for the
   * hash signature.
   * For the signature each value must be suffixed with the hash key
   */
  $sha1suffix = variable_get('directlink_sha_in');
  $request_to_hash = '';
  $request_string = '';

  foreach ($data as $name => $value) {
    // do not send empty values
    if (!empty($value)) {
      // Build the string from all available parameters
      $request_to_hash .= $name . '=' . $value . $sha1suffix;
      $request_string .= $name . '=' . $value . '&';

    }
  };

  $postdata = $request_string . 'SHASIGN=' . sha1($request_to_hash);

  /**
   * 3. Return the string fo use in commerce_ogone_alias_directlink_payment function
   */
  return ($postdata);
}

/**
 * Hardcoded test function to send a test payment transaction
 * Can be used to check if Ogone DirectLink configuration is ok.
 * Run it from url "commerce_ogone_alias/test"
 * @return string
 */
function commerce_ogone_alias_directlink_payment_test() {

  watchdog('Commerce Ogone Alias', "Test Payment Transaction started");

  global $user;
  $uid = $user->uid ;

  global $language;
  $payment_language = $language->language;

  $url = variable_get('ogone_directlink_url_test');

  /**
   * Set test data for the transaction
   */
  $data = array(
    // General parameters
    'PSPID' => variable_get('directlink_pspid'),
    'ORDERID' => rand(1, 10000),
    'AMOUNT' => rand(5, 100000),
    'USERID' => variable_get('ogone_directlink_api_user'),
    'PSWD' => variable_get('ogone_directlink_api_pswd'),
    'CURRENCY' => variable_get('commerce_default_currency', 'EUR'),
    'LANGUAGE' => $payment_language,
    'ALIAS' => t(ALIAS_TEMPLATE, array('@uid' => $uid)),
  );

  // Ogone expects sorted data
  ksort($data);

  /**
   * Each value must be suffixed with the hash key
   */

  $sha1suffix = variable_get('directlink_sha_in');
  $request_to_hash = '';
  $request_string = '';

  foreach ($data as $name => $value) {
    // do not send empty values
    if (!empty($value)) {
      // Build the string from all filled in parameters
      $request_to_hash .= $name . '=' . $value . $sha1suffix;
      $request_string .= $name . '=' . $value . '&';
    }
  };

  $postdata = $request_string . 'SHASIGN=' . sha1($request_to_hash);

  $response = drupal_http_request($url, array(
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      'data' => $postdata,
      'method' => 'POST',
      'timeout' => 10,
    )
  );

  if (!empty ($response->status_message) && $response->status_message == 'OK') {

    $payment_transaction_data = new SimpleXMLElement($response->data);
    $payment_transaction_data = drupal_json_decode(drupal_json_encode($payment_transaction_data));

    /**
     * turn result in a table for a clean test result page
     */
    $header = array(t ('Key'), t ('Value'),);
    $rows = array();
    foreach ( $payment_transaction_data['@attributes'] as $key => $value ) {
      $rows[] = array ($key, $value) ;
    }

    $result_html = t ("<h2>Request sent to Ogone</h2>
                        <p>$postdata</p>
                    <h2>Answer from Ogone</h2>" ) .
                      theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $result_html = t ("<p>Payment Request did reach ogone :-(</p><p>" ) .
    l( "Check your configuration",
      '/admin/commerce/config/payment-methods/commerce_ogone_alias',
      array ('attributes' => array ('class' => 'button'))) .
      "</p>" ;
  };

  return $result_html ;

  watchdog('Commerce Ogone Alias', "Test Payment Transaction finished");

}

